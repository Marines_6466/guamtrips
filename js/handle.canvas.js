/**
 * Created by quangdv.90 on 12/27/2016.
 */
function loadImageThumb(divWrap, divThumb, imageThumbView, divContainer, divBlur) {
    var divImage = divWrap;
    var divThumb = divThumb;

    var wthumb = divThumb.width();
    var hthumb = divThumb.height();

    // console.log(divThumb.offset());

    var xthumb = divThumb.offset().left - divImage.offset().left;
    var ythumb = divThumb.offset().top - divImage.offset().top;

    /*divContainer.attr('style', "visibility: hidden;");
     divBlur.hide();*/

    html2canvas(divImage[0], {
        onrendered: function(canvas) {
            var context = canvas.getContext("2d");
            var data = context.getImageData(xthumb, ythumb, wthumb, hthumb);

            imageThumbView.attr('src', dataToImage(data));

            /*divContainer.attr('style', "visibility: visible;");
             divBlur.show();*/
        }
    });

}

function dataToImage(imagedata) {
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    canvas.width = imagedata.width;
    canvas.height = imagedata.height;
    ctx.putImageData(imagedata, 0, 0);

    var image = new Image();
    return canvas.toDataURL();
}